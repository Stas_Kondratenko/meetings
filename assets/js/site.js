var englishBtn = document.getElementById("english-btn");
var russianBtn = document.getElementById("russian-btn");
var englishForm = document.getElementById("english-form");
var russianForm = document.getElementById("russian-form");

document.addEventListener('DOMContentLoaded', function() {
 
englishBtn.addEventListener('click', function() {
   
  englishForm.submit();

}, false);

russianBtn.addEventListener('click', function() {
   
  russianForm.submit();

}, false);

 
});
