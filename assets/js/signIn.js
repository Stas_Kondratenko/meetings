var passwordRegex = /^[а-яА-ЯёЁa-zA-Z0-9]{5,20}$/;
var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

document.addEventListener('DOMContentLoaded', function() {
 
   document.getElementById("signin_btn").addEventListener('click', function() { 
    if(IsValideValue(document.getElementById("email").value, emailRegex)){
        
        if(IsValideValue(document.getElementById("password").value, passwordRegex)){
            document.getElementById("signin-form").submit();  
        }
        else{
            
           document.getElementById("error").innerHTML = "Неверный пароль. Пароль должен состоять из не менее чем 5 символов и не может включать спецсимволы!";
        }      
    }
    else {
        document.getElementById("error").innerHTML = "Неверный email. Email должен быть в формате user@host.com!";
    }
    
  }, false);

 document.getElementById("english-btn").addEventListener('click', function() {
  document.getElementById("english-form").submit();
}, false);

 document.getElementById("russian-btn").addEventListener('click', function() {
  document.getElementById("russian-form").submit();
}, false);
 
});

    function IsValideValue(login, regex){
        if(login.match(regex)){return true;}
        else{return false;}
     }