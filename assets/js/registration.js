var errors = Array();

document.addEventListener('DOMContentLoaded', function() {
 
   document.getElementById("registration-btn").addEventListener('click', function() { 
       
       
       if(document.getElementById("password").value !== null && document.getElementById("password").value !== '' && document.getElementById("password").value === document.getElementById("confirmPassword").value){
            IsValideEmail(document.getElementById("email").value);
            IsValidePassword(document.getElementById("password").value);
            IsValideString(document.getElementById("lastName").value);
            IsValideString(document.getElementById("name").value);
            IsValidePhone(document.getElementById("phone").value);
            IsValideDate(document.getElementById("birthday").value);
            IsValideString(document.getElementById("experience").value);
            IsValideString(document.getElementById("education").value);
            IsValideString(document.getElementById("about").value);
            IsValideString(document.getElementById("adress").value);
          
            if(errors.length == 0){ 
                document.getElementById("registration-form").submit();  
            }
            else{
                 document.getElementById("error").innerHTML = '';
                 for(var i = 0; i < errors.length; i++)
                     {   
                         var textElem = document.createTextNode(errors[i]);
                         var br = document.createElement("br");
                         var hr = document.createElement("hr");
                         document.getElementById("error").appendChild(textElem);
                         document.getElementById("error").appendChild(br);
                         document.getElementById("error").appendChild(hr);
                     } 
                 errors.length = 0;
            }  
        }
        else{
            document.getElementById("error").innerHTML = "Пароли не совпадают!"; 
        }
            
  }, false);

  document.getElementById("english-btn").addEventListener('click', function() {
  document.getElementById("english-form").submit();
}, false);

  document.getElementById("russian-btn").addEventListener('click', function() {
  document.getElementById("russian-form").submit();
}, false);
 
});

    function IsValideEmail(value){
        if(!value.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)){
           errors[errors.length] = "Неверный email. Email должен быть в формате user@host.com!";
        }  
     }
     
    function IsValidePassword(value){
        if(!value.match(/^[а-яА-ЯёЁa-zA-Z0-9]{5,20}$/)){
           errors[errors.length] = "Неверный пароль. Пароль должен состоять из не менее чем 5 символов и не может включать спецсимволы!";
        }  
     }
     
    function IsValideString(value){
       var itWas = true;
       if(value === "" || value === null)
       {  
           for(var i = 0; i < errors.length; i++){  
                     if(errors[i] == "Все поля обязательны для заполнения!"){
                        itWas = false;
                        break;
                     }
            }
            if(itWas){
                errors[errors.length] = "Все поля обязательны для заполнения!" ;
            }       
       }
     }
     
    function IsValidePhone(value){
       if(!value.match(/^[0-9]+$/) && value.length < 5){
          errors[errors.length] = "Номер телефона должен состоять из не менее чем 6 цифр!";
        }  
     }
     
     function IsValideDate(value){
         if(value.length !== 10){
             errors[errors.length] = "Поле даты заполнено неверно!";      
         } 
     }
     
     
