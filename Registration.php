<?php require_once __DIR__.'/inc/initialization.inc.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Meetings registration</title>

    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/registration.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css">
    <link href="assets/image/favicon.png" rel="icon">

</head>
<body>

     <nav class="navbar">
        <div class="container-fluid">

            <div class="navbar-header" id="logo">
                <img src="assets/image/favicon.png" >
            </div>

            <div class="collapse navbar-collapse">

                    <ul class=" nav navbar-nav" id="navbarUL">
                        <li class=""> <a href="index.php"><?php echo $local["home"]?></a> </li>
                        <li class=""> <a href="SignIn.php"><?php echo $local["signIn"]?></a></li>
                        <li class="active"> <a href="Registration.php"><?php echo $local["registration"]?></a> </li>
                    </ul>

                <ul class="nav navbar-nav" id="language">
                        <form method="get" id="russian-form">
                            <input type="hidden" name="language" value="ru">
                        </form>
                        <form method="get" name="language" id="english-form">
                            <input type="hidden" name="language" value="en">
                        </form>
                        <li class=""><a href="#" id="russian-btn">ru</a></li>
                        <li class=""><a href="#" id="english-btn">en</a></li>
                    </ul>
            </div>
            
        </div>
    </nav>
    
    
    <section class="container">
        <section class="login-form">
            
            <form class="form-horizontal" enctype="multipart/form-data" action="Registration.php" role="login" method="post" id="registration-form"> 
                
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["lastName"]?></label>
                    <div class="col-xs-8">
                      <input type="text" class="form-control" name="lastname" id="lastName" placeholder="<?php echo $local["lastName"]?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["name"]?></label>
                    <div class="col-xs-8">
                      <input type="text" class="form-control" name="name" id="name" placeholder="<?php echo $local["name"]?>">
                    </div>
                  </div>

                  <div class="form-group">
                        <label class="control-label col-xs-4"><?php echo $local["birthday"]?></label>
                        <div class="col-xs-8">
                            <input type="date" class="form-control col-xs-4" name="birthday" id="birthday">
                        </div>

                    </div>

                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["adress"]?></label>
                    <div class="col-xs-8">
                      <input type="text" class="form-control" id="adress" name="adress" placeholder="<?php echo $local["adress"]?>">
                    </div>
                  </div>
                
                 <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["status"]?></label>
                    <div class="col-xs-4">
                      <label class="radio-inline">
                        <input type="radio" name="status" checked="" value="free"><?php echo $local["free"]?>
                      </label>
                    </div>
                    <div class="col-xs-4">
                      <label class="radio-inline">
                        <input type="radio" name="status" value="maried"><?php echo $local["married"]?>
                      </label>
                    </div>
                  </div>

                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["education"]?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="education" id="education" placeholder="<?php echo $local["education"]?>">
                    </div>
               </div>

                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["experience"]?></label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" name="experience" id="experience" placeholder="<?php echo $local["experience"]?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["email"]?>:</label>
                    <div class="col-xs-8">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                    </div>
                  </div>

                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["phone"]?></label>
                    <div class="col-xs-8">
                        <input type="tel" class="form-control" name= "phone" id="phone" placeholder="<?php echo $local["phone"]?>">
                    </div>
                  </div> 

                 <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["about"]?></label>
                    <div class="col-xs-8">
                      <textarea rows="2" class="form-control" name = "about" id="about" placeholder="<?php echo $local["aboutTell"]?>"></textarea>
                    </div>
                  </div>
                
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["password"]?>:</label>
                    <div class="col-xs-4">
                      <input type="password" class="form-control" name="password" id="password" placeholder="<?php echo $local["password"]?>">
                    </div>
                    
                    <div class="col-xs-4">
                      <input type="password" class="form-control" name= "confirmPassword" id="confirmPassword" placeholder="<?php echo $local["repeat"]?>">
                    </div>
                  </div>
                
                
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["image"]?></label>
                    <div class="col-xs-8">
                      <input type="hidden" name="MAX_FILE_SIZE" value="4000000"/>
                      <input type="file" name="image" class="form-control" id="image" accept="image/gif,image/jpeg,image/png">
                    </div>
                  </div>
                
                  <div class="form-group">
                       <div class="col-xs-6">
                           <button type="reset" name="go" class="btn btn-block btn-danger" id="reset"><?php echo $local["reset"]?></button>
                       </div>
                       <div class="col-xs-6">
                       <button type="button" name="registration-btn" class="btn btn-block btn-success" id="registration-btn"><?php echo $local["registration"]?></button>
                       </div>
                  </div>
                
                 <div class="error">
                     <p id="error"> </p>
                     <?php require "inc/registration.inc.php"; ?>
                 </div>
     
            </form>
                
        </section>
    </section>

<script src="assets/js/registration.js"></script>
</body>
</html>