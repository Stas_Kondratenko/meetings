<?php require_once __DIR__.'/inc/initialization.inc.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Meetings</title>

    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/signinform.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css">
    <link href="assets/image/favicon.png" rel="icon">

</head>
<body>

     <nav class="navbar">
        <div class="container-fluid">

            <div class="navbar-header" id="logo">
                <img src="assets/image/favicon.png" >
            </div>

            <div class="collapse navbar-collapse">

                    <ul class=" nav navbar-nav" id="navbarUL">
                        <li class=""> <a href="index.php"><?php echo $local["home"]?></a> </li>
                        <li class="active"> <a href="SignIn.php"><?php echo $local["signIn"]?></a></li>
                        <li class=""> <a href="Registration.php"><?php echo $local["registration"]?></a> </li>
                    </ul>

                    <ul class="nav navbar-nav" id="language">
                        <form method="get" id="russian-form">
                            <input type="hidden" name="language" value="ru">
                        </form>
                        <form method="get" name="language" id="english-form">
                            <input type="hidden" name="language" value="en">
                        </form>
                        <li class=""><a href="#" id="russian-btn">ru</a></li>
                        <li class=""><a href="#" id="english-btn">en</a></li>
                    </ul>
            </div>

        </div>
    </nav>

  <section class="container">
        <section class="login-form">
            <form method="post" role="login" id="signin-form" action="SignIn.php">

                <label><?php echo $local["email"]?></label>
                <input type="text" name="email" required class="form-control" id="email" />

                <label><?php echo $local["password"]?></label>
                <input type="password" name="password" required class="form-control" id="password" />

                <span> <?php echo $local["remember"]?> <input type="checkbox" name="remember" class="form-control checkbox" value="true"/></span>

                <button type="button" name="go" class="btn btn-block btn-danger" id="signin_btn"><?php echo $local["signIn"]?></button>

                <div class="error">
                    <p id="error"></p>
                         <?php require "inc/signIn.inc.php"; ?>
                </div>

            </form>
        </section>
    </section>

<script src="assets/js/signIn.js"></script>
</body>
</html>