<?php
class Validation{
    
    function IsValidEmail($email){
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            return $this->Clean($email);
        }
        else{ $GLOBALS["errors"][] = "Неверный email. Email должен быть в формате user@host.com!"; } 
    }
    
    function IsValidImage($image,$size){
        if($size > 4*1024*1024||$size==0)
         {
          $GLOBALS["errors"][]= "В качестве аватара может быть изображения форматов gif, jpg, png. Размер изображения не должен прeвышать 2Мб!";
         }
         else{
            return $image;            
         }  
    }
    
    function IsValidString($string){
           $string =  trim(strip_tags($string));
           if($string != null || $string !=""){
           return $this->Clean($string);
        }
        else{
            if(!in_array("Все поля обязательны для заполнения!",$GLOBALS["errors"]))
            { $GLOBALS["errors"][] = "Все поля обязательны для заполнения!";}
        }     
    }
    
    function IsValidBool($bool){
       if($bool != null && $bool !=""){
            return $bool = true; 
       }
       else{
            return $bool = false; 
       }
    }
    
    function IsValidPhone($phone){
         $regexp = '/^[0-9]+$/';
         if(preg_match($regexp,$phone)&& iconv_strlen($phone)>5 ){
         return $this->Clean($phone);
         }
         else{   
          $GLOBALS["errors"][] = "Номер телефона должен состоять из не менее чем 6 цифр!";   
         }     
     }
     
    function IsValidDate($date){
         if(iconv_strlen($date)==10){
         return $this->Clean($date);
         }
         else{   
          $GLOBALS["errors"][] = "Поле даты заполнено неверно!";   
        }     
     }
    
    function IsValidPassword($password,$confirmPassword){
        
        $regexp = '/^[а-яА-ЯёЁa-zA-Z0-9]+$/';
        if($password===$confirmPassword){
            if(preg_match($regexp,$password)&& iconv_strlen($password)>=5 ){
                return $this->Clean($password);
            }
            else{ $GLOBALS["errors"][] = "Неверный пароль. Пароль должен состоять из не менее чем 5 символов и не может включать спецсимволы!"; } 
        }
        else{
            $GLOBALS["errors"][] = "Пароли не совпадают!";
        }
               
    }
    
    function Clean($value) {
        $value = trim($value);
        $value = strip_tags($value);
        $value = mysql_escape_string($value);
        return $value;
    }
}
    
 