<?php
    require 'UserManager.class.php';
    class Authentication extends UserManager{

        function Registration($user) {
           $user->password = $this->GetHash($user->password);
           if($this->CreateUser($user)){
                $this->LogIn($user);
                return true;
           }
           else
           {return false;}
        }

        function LogIn($user){
            $user = $this->IsValidAuth($user); 
            if($user != false){
                if ($user->isRemember == "true"){$this->SendCookie($user->email, $user->password); }
                $this->SetSession($user);
                header("Location: Site.php");
            }
            else{
                return false;
            }
        }

        function LogInByCookie($email,$password){
            $user = new User($email,$password,true);
            $user = $this->IsValidAuth($user);
            if($user != false){
               $this->SetSession($user);
               $this->SendCookie($email,$password);
               echo $user->name;
               header("Location: Site.php");
               return true;
            }
            else {
               return false;
            }
        }

        function LogOut(){
            setcookie("user","",time()-3600*24,"/");
            header("Location: index.php");
        }

        function SendCookie($email, $password){
            $user = array(
                        "email" => $email,
                        "password" => $password
                    );
                    $str = base64_encode(serialize($user));
                    setcookie("user",$str,time()+3600*24,"/");
        }

        function GetHash($password) {
            return md5($password."meetings");
        }

        function SetSession($user){
           $_SESSION["user"]["name"] = $user->name;
           $_SESSION["user"]["lastName"] = $user->lastName;
           $_SESSION["user"]["email"] = $user->email;
           $_SESSION["user"]["status"] = $user->status;
           $_SESSION["user"]["birthday"] = $user->birthday;
           $_SESSION["user"]["about"] = $user->about;
           $_SESSION["user"]["adress"] = $user->adress;
           $_SESSION["user"]["education"] = $user->education;
           $_SESSION["user"]["experience"] = $user->experience;
           $_SESSION["user"]["phone"] = $user->phone;
           $_SESSION["user"]["image"] = $user->image;
        }
    }