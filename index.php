<?php require_once __DIR__.'/inc/initialization.inc.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Meetings</title>

    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css">
    <link href="assets/image/favicon.png" rel="icon">

</head>
<body>
    <div class="container container-table">  
        <div class="homepage-info row vertical-center-row">
            <div class="text-center col-md-6 col-md-offset-3"> 
                <img src="assets/image/bigLogo.png"/>
                <span class="logo-text">Meetings</span>
                <p><?php echo $local["logo"]?></p>
                <a id="signin-btn" href="SignIn.php"><?php echo $local["signIn"]?></a>
            </div>     
       </div>
    </div>
    
    <footer class="navbar-fixed-bottom row-fluid">
            <div class="container text-center" style="color:white;">
                <p>Copyright © Meetings 2015</p>
            </div>
    </footer>
 
</body>
</html>