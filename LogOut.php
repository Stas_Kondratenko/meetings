<?php
session_start();
if(isset($_SESSION["user"])){
  unset($_SESSION["user"]);   
}

require "logic/Authentication.class.php";

$logOut = new Authentication();
$logOut->LogOut();