<?php
    $errors = array();
    global $errors;

if(!empty($_POST)){
    $valid = new Validation();

    $email = $valid->IsValidEmail($_POST["email"]);
    $password = $valid->IsValidPassword($_POST["password"],$_POST["confirmPassword"]);

    $user = new User($email,$password,false);

    $user->name = $valid->IsValidString($_POST["name"]);
    $user->status = $valid->IsValidString($_POST["status"]);
    $user->lastName = $valid->IsValidString($_POST["lastname"]);
    $user->birthday = $valid->IsValidDate($_POST["birthday"]);
    $user->adress = $valid->IsValidString($_POST["adress"]);
    $user->education = $valid->IsValidString($_POST["education"]);
    $user->experience = $valid->IsValidString($_POST["experience"]);
    $user->phone = $valid->IsValidPhone($_POST["phone"]);
    $user->about = $valid->IsValidString($_POST["about"]);

      if (!empty($_FILES['image']["tmp_name"]))
        {
           $user->image = base64_encode($valid->IsValidImage(file_get_contents($_FILES['image']["tmp_name"]),$_FILES['image']["size"]));
        }
     if(empty($errors))
     {
        $auth = new Authentication();

        if(!$auth->Registration($user)){
           echo 'Пользователь с таким email уже существует!';
        }
     }
     else{
        foreach($GLOBALS["errors"] as $error)
        {
            echo $error."<br><hr>";
        }
        unset($GLOBALS["errors"]);
    }
}