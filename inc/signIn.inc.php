<?php 

$errors = array();
global $errors;

if(!empty($_POST)){
    $valid = new Validation();
 
    $email = $valid->IsValidEmail($_POST["email"]);
    $password = $valid->IsValidPassword($_POST["password"],$_POST["password"]);
    $isRemember= $valid->IsValidBool(isset($_POST["remember"]));
    
    if(empty($errors))
    {
       $auth = new Authentication();
       
       $user = new User($email,$auth->GetHash($password),$isRemember);
       $user = $auth->LogIn($user);
       if(!$user){
          echo "Пользователя с таким email и логином не существует!";
       }
 }
 else{
        foreach($GLOBALS["errors"] as $error)
        {
            echo $error."<br><hr>";  
        } 
        unset($GLOBALS["errors"]);
 }      
}

