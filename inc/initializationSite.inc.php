<?php
session_start();
if(!isset($_SESSION["user"])){
    header("Location: SignIn.php");
}

if(isset($_GET["language"])){
    $_SESSION["localization"] = trim(strip_tags($_GET["language"]));   
}

if(isset($_SESSION["localization"])){
    require "local/".$_SESSION["localization"].".php";   
}
else{
    require "local/en.php";  
}