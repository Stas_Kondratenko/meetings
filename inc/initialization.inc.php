<?php
require "models/user.php";
require "logic/Authentication.class.php";
require "logic/Validation.class.php";

session_start();
ob_start();

if(isset($_COOKIE["user"])){
   $cookie = base64_decode($_COOKIE["user"]);
   $cookieValue = unserialize($cookie);
   $auth = new Authentication();
   $auth->LogInByCookie($cookieValue["email"],$cookieValue["password"]);
}

if(isset($_GET["language"])){
    $_SESSION["localization"] = trim(strip_tags($_GET["language"]));
}

if(isset($_SESSION["localization"])){
    require "local/".$_SESSION["localization"].".php";
}
else{
    require "local/en.php";
}