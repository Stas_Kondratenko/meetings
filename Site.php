<?php require_once __DIR__.'/inc/initializationSite.inc.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Meetings registration</title>

    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/site.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css">
    <link href="assets/image/favicon.png" rel="icon">

</head>
<body>  
    
    <nav class="navbar">
        <div class="container-fluid">

            <div class="navbar-header" id="logo">
                <img src="assets/image/favicon.png" >
            </div>
            
            <div class="collapse navbar-collapse">
             
                    <ul class=" nav navbar-nav" id="navbarUL">
                        <li class="active"> <a href="#"><?php echo $local["form"]?></a></li>
                    </ul>

                    <ul class="nav navbar-nav" id="language">
                            <form method="get" id="russian-form">
                                <input type="hidden" name="language" value="ru">
                            </form>
                            <form method="get" name="language" id="english-form">
                                <input type="hidden" name="language" value="en">   
                            </form>
                            <li class=""><a href="#" id="russian-btn">ru</a></li>
                            <li class=""><a href="#" id="english-btn">en</a></li>   
                    </ul>
                
                     <ul class="nav navbar-nav" id="logOutList">
                        <li class=""><a href="LogOut.php"><?php echo $local["logOut"]?></a></li>
                    </ul>
                
            </div>
            
            
         
        </div>
   </nav>
    
    
    <section class="container">
        <section class="login-form">
            
            <div class="form-horizontal" role="login">
                
                 <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["image"]?></label>
                    <div class="col-xs-8">
                        <img src="data:image/jpg;base64,<?php echo $_SESSION['user']['image']?>" style="width:100%" />
                    </div>
                  </div>
                
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["lastName"]?></label>
                    <div class="col-xs-8">
                      <label class="control-label"><?php echo $_SESSION["user"]["lastName"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                    
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["name"]?></label>
                    <div class="col-xs-8">
                      <label class="control-label"><?php echo $_SESSION["user"]["name"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                    
                  <div class="form-group">
                        <label class="control-label col-xs-4"><?php echo $local["birthday"]?></label>
                        <div class="col-xs-8">
                            <label class="control-label"><?php echo $_SESSION["user"]["birthday"]?></label>
                        </div>
                        
                    </div>
                  
                  <hr>
                  
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["adress"]?></label>
                    <div class="col-xs-8">
                         <label class="control-label"><?php echo $_SESSION["user"]["adress"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                
                 <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["status"]?></label>
                    <div class="col-xs-8">
                         <label class="control-label"><?php echo $_SESSION["user"]["status"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                  
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["education"]?></label>
                    <div class="col-xs-8">
                        <label class="control-label"><?php echo $_SESSION["user"]["education"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["experience"]?></label>
                    <div class="col-xs-8">
                        <label class="control-label"><?php echo $_SESSION["user"]["experience"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                
                  <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["email"]?>:</label>
                    <div class="col-xs-8">
                        <label class="control-label"><?php echo $_SESSION["user"]["email"]?></label>
                    </div>
                  </div>
                  
                  <hr>
                
                <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["phone"]?></label>
                    <div class="col-xs-8">
                        <label class="control-label"><?php echo $_SESSION["user"]["phone"]?></label>
                    </div>
                  </div> 
                  
                  <hr>
                
                 <div class="form-group">
                    <label class="control-label col-xs-4"><?php echo $local["about"]?></label>
                    <div class="col-xs-8">
                        <label class="control-label"><?php echo $_SESSION["user"]["about"]?></label>
                    </div>
                  </div>   
                
            </div>
                
        </section>
    </section>
    <script src="assets/js/site.js"></script>
</body>
</html>
